#!/usr/bin/env python
import cv2
import csv
import roslib
import rospy
import math
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from cv_bridge import CvBridge, CvBridgeError
from datetime import datetime
from sensor_msgs.msg import Image
from signal import signal, SIGINT

import tf

# target_frame_id_list[i] is pair with parent_frame_id_list[i]
target_frame_id_list = ["ar_marker/719"] # full name
parent_frame_id_list = ["ceiling_camera_link"] # full name

# target_frame_candidate_list[i] is independent on parent_frame_candidate_list[i]
# look up all combination of target_frame_candidate_list[i] and parent_frame_candidate_list[j]
target_frame_candidate_list = ["ar_marker"] # header
parent_frame_candidate_list = ["ceiling_camera_link"] # full name

subscribed_topic = "/hk/ceiling_camera_link/image_raw"
tf_topic = "/tf"

csv_header = ["x[m]", "y[m]", "z[m]", "roll[deg]", "pitch[deg]", "yaw[deg]"]

output_path="/home/kei_yoshikawa/TMH_marker_recognition/20200213/test"



class TfRecorder(object):
    def __init__(self, output_path, subscribed_topic, description):

        self.save_images = True
        self.write_summary = True

        ### set parameters start ###
        self.subscribed_topic = subscribed_topic

        ## set output paths and create directories
        self.description = datetime.now().strftime("%Y%m%d") + "_" + description
        self.output_dir = output_path + "/" + self.description + "/"
        self.output_imagme_dir = self.output_dir + "images"
        self.output_csv = self.output_dir+ self.description + ".csv"
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if not os.path.exists(self.output_imagme_dir) and self.save_images:
            os.makedirs(self.output_imagme_dir)

        ## set tf setting
        self.target_frame_id_list = target_frame_id_list
        self.parent_frame_id_list = parent_frame_id_list
        self.target_frame_candidate_list = target_frame_candidate_list
        self.parent_frame_candidate_list = parent_frame_candidate_list

        self.time_start = None
        self.time_last = None
        self.check_realtime = True

        self.calc_item_header = csv_header
        self.frame_count = 0
        self.time_list = []
        # success rate
        self.success_counts = []
        for i in range(len(self.target_frame_id_list)):
            self.success_counts.append(0)
        # translation and rotation
        self.translations = []
        self.rotations = []
        self.trans_list = []
        self.rot_list = []
        ### set parameters end   ###

        ### init output file start ###
        self.save_individual_result = True
        self.write_fp = csv.writer(open(self.output_csv, "a"))
        print('Open file: ' + str(self.output_csv))
        ### init output file end   ###

        print('Initialization done.')

    def MsgCallback(self, msg, listener):
        time = msg.header.stamp
        if self.time_start == None:
            self.time_start = time
            self.time_last = time
        print((time - self.time_start).to_sec(), " s")
        self.time_list.append((time - self.time_start).to_sec())

        self.UpdateFrameIdList(listener, time=time)
        self.UpdateTf(listener, time=time)

        find_tf = self.WriteResult(time)

        if self.save_images:
            try:
                if find_tf:
                    cv_image = CvBridge().imgmsg_to_cv2(msg, "bgr8")
                    fname = self.output_imagme_dir + "/" + "{:04}".format(self.frame_count) + ".jpg"
                    cv2.imwrite(fname, cv_image)

                    self.frame_count += 1
            except CvBridgeError as e:
                print(e)
        else:
            self.frame_count += 1

        self.time_last = time

        return

    def UpdateFrameIdList(self, listener, time=None):
        if time==None:
            time=rospy.time(0)
        else:
            listener.waitForTransform(self.parent_frame_id_list[0], self.parent_frame_id_list[0],
                                      time, rospy.Duration(10.0))
        frame_id_list = listener.getFrameStrings()

        for frame_id in frame_id_list:
            registered_id = False
            # check if it is registered as target
            for target_frame_id in self.target_frame_id_list:
                if frame_id == target_frame_id:
                    registered_id = True
                    break

            # check if it is a candidate
            if registered_id:
                continue
            else:
                # for i in range(len(self.target_frame_candidate_list)):
                for target_frame_candidate in self.target_frame_candidate_list:
                    if registered_id:
                        break
                    else:
                        if target_frame_candidate in frame_id:
                            for parent_frame_candidate in self.parent_frame_candidate_list:
                                try:
                                    # check if its tf is avairable
                                    parent_frame_id = parent_frame_candidate
                                    (trans,rot) = listener.lookupTransform(parent_frame_id, frame_id, time)
                                    print("t: ", trans, rot)
                                except:
                                    continue

                                self.target_frame_id_list.append(frame_id)
                                self.parent_frame_id_list.append(parent_frame_id)
                                self.success_counts.append(0)
                                registered_id = True
                                break
        return

    def UpdateTf(self, listner, time=None):
        for i in range(len(self.target_frame_id_list)):
            target_frame_id = self.target_frame_id_list[i]
            parent_frame_id = self.parent_frame_id_list[i]
            print(target_frame_id)

            while not rospy.is_shutdown():
                try:
                    if time==None:
                        listener.waitForTransform(parent_frame_id, target_frame_id, rospy.time(0))
                        (trans, rot_q) = listener.lookupTransform(parent_frame_id, target_frame_id, rospy.time(0))
                    else:
                        if self.check_realtime and self.time_start < self.time_last:
                            duration = rospy.Duration((time - self.time_last).to_sec())
                        else:
                            duration = rospy.Duration(1.0)
                        listener.waitForTransform(parent_frame_id, target_frame_id,
                                                  time, duration)
                        (trans, rot_q) = listener.lookupTransform(parent_frame_id, target_frame_id, time)
                    rot = tf.transformations.euler_from_quaternion(rot_q)
                    self.success_counts[i] += 1
                except:
                    trans = [np.nan, np.nan, np.nan]
                    rot = [np.nan, np.nan, np.nan]
                break

            print(str(trans), str(rot))
            self.translations.append(np.array(trans, dtype=float))
            rad_to_deg = 180 / 3.141592635
            rad_to_deg_array = np.array([rad_to_deg, rad_to_deg, rad_to_deg])
            rot_deg = np.multiply(np.array(rot, dtype=float), rad_to_deg_array)
            self.rotations.append(rot_deg)

        return

    def WriteResult(self, time_img):
        results = []
        # frame count
        results.append(self.frame_count)

        # time
        time = (time_img - self.time_start).to_sec()
        results.append(time)

        # blank
        results.append("")

        find_tf = False
        # trans, rot
        for i in range(len(self.target_frame_id_list)):
            results.append("") # frame_id raw
            try:
                trans = self.translations[i]
                for j in range(0, 3):
                    if trans is not None:
                        results.append(trans[j])
                    else:
                        results.append("")

                rot = self.rotations[i]
                for j in range(0, 3):
                    if rot is not None:
                        results.append(rot[j])
                    else:
                        results.append("")

                find_tf = True

            except:
                for j in range(0, 6):
                    results.append("")

            results.append("") # blank raw

        # write csv
        self.write_fp.writerow(results)

        # prepare for next tf
        self.trans_list.append(self.translations)
        self.rot_list.append(self.rotations)

        self.translations = []
        self.rotations = []

        return find_tf

    def CompensateNoDetectedItem(self):
        # compensate blank (no marker detected)
        # in case no marker detected
        if len(self.trans_list) == 0 and len(self.rot_list) == 0:
            no_values = []
            for i in range(len(self.target_frame_id_list)):
                no_values.append([0, 0, 0])

            self.trans_list.append(no_values)
            self.rot_list.append(no_values)
        else:
            # in case no tf was detected for each marker
            size = len(self.target_frame_id_list)
            for i in range(len(self.trans_list)):
                for j in range(0, (size - len(self.trans_list[i]))):
                    self.trans_list[i].append(np.array([np.nan, np.nan, np.nan]))
                    self.rot_list[i].append(np.array([np.nan, np.nan, np.nan]))
        self.trans_arrays = np.array(self.trans_list)
        self.rot_arrays = np.array(self.rot_list)

        # check if at least one tf was detedted
        self.detected_tf_index = []
        for i in range (len(self.target_frame_id_list)):
            self.detected_tf_index.append(False)

        for trans_array in self.trans_arrays:
            for i in range(len(self.target_frame_id_list)):
                if self.detected_tf_index[i]:
                    continue
                if ~np.isnan(trans_array).all(axis=1)[i]:
                    self.detected_tf_index[i] = True

        # remove no tf detected marker
        detected_trans_arrays = np.copy(self.trans_arrays)
        detected_rot_arrays = np.copy(self.rot_arrays)
        for i in range(len(self.target_frame_id_list)):
            if not self.detected_tf_index[i]:
                detected_trans_arrays = np.delete(self.trans_arrays, i, 1)
                detected_rot_arrays = np.delete(self.rot_arrays, i, 1)

        return detected_trans_arrays, detected_rot_arrays

    def CalcDetectionRate(self):
        detected_rate_list = []
        for i in range(len(self.target_frame_id_list)):
            if self.frame_count == 0:
                sr = 0
            else:
                sr = float(self.success_counts[i]) / self.frame_count
            detected_rate_list.append(sr)
        return detected_rate_list

    def StrTransAndRot(self, trans_array, rot_array):
        line = str(trans_array[0]) + "," + str(trans_array[1]) + "," + str(trans_array[2]) + "," \
               + str(rot_array[0]) + "," + str(rot_array[1]) + "," + str(rot_array[2]) + ","
        return line

    def WriteFinalResults(self):
        blank_line = " , , ," # frame_count, time, blank
        success_rate_line = "Total frames," + str(self.frame_count) + ", ,"
        average_line = blank_line
        std_dev_line = blank_line
        min_line = blank_line
        max_line = blank_line

        # calc results
        # success_rate
        self.success_rate_list = self.CalcDetectionRate()

        try:
            detected_trans_array, detected_rot_array = self.CompensateNoDetectedItem()

            # average
            average_trans = np.nanmean(detected_trans_array, axis=0)
            average_rots = np.nanmean(detected_rot_array, axis=0)
            #std
            std_dev_trans = np.nanstd(detected_trans_array, axis=0)
            std_dev_rots = np.nanstd(detected_rot_array, axis=0)
            # min
            min_trans = np.nanmin(detected_trans_array, axis=0)
            min_rots = np.nanmin(detected_rot_array, axis=0)
            # max
            max_trans = np.nanmax(detected_trans_array, axis=0)
            max_rots = np.nanmax(detected_rot_array, axis=0)
        except:
            self.detected_tf_index = []
            for i in range (len(self.target_frame_id_list)):
                self.detected_tf_index.append(False)

        # prepare result lines
        index = 0
        self.ave_trans_list = []
        self.ave_rot_list = []
        self.std_trans_list = []
        self.std_rot_list = []
        self.min_trans_list = []
        self.min_rot_list = []
        self.max_trans_list = []
        self.max_rot_list = []
        for i in range(len(self.target_frame_id_list)):
            if self.detected_tf_index[i]:
                self.ave_trans_list.append(average_trans[index])
                self.ave_rot_list.append(average_rots[index])
                self.std_trans_list.append(std_dev_trans[index])
                self.std_rot_list.append(std_dev_rots[index])
                self.min_trans_list.append(min_trans[index])
                self.min_rot_list.append(min_rots[index])
                self.max_trans_list.append(max_trans[index])
                self.max_rot_list.append(max_rots[index])
                index += 1
            else:
                nan_array = np.asarray([np.nan, np.nan, np.nan], dtype=float)
                self.ave_trans_list.append(nan_array)
                self.ave_rot_list.append(nan_array)
                self.std_trans_list.append(nan_array)
                self.std_rot_list.append(nan_array)
                self.min_trans_list.append(nan_array)
                self.min_rot_list.append(nan_array)
                self.max_trans_list.append(nan_array)
                self.max_rot_list.append(nan_array)

        for i in range(len(self.target_frame_id_list)):
            success_rate_line = success_rate_line + "Success rate," + str(self.success_rate_list[i]) + ", ," \
                                + "Detected frames," + str(self.success_counts[i]) + ", , , ,"
            average_line = average_line + "Average," \
                           + self.StrTransAndRot(self.ave_trans_list[i], self.ave_rot_list[i]) + ","
            std_dev_line = std_dev_line + "Std dev," \
                           + self.StrTransAndRot(self.std_trans_list[i], self.std_rot_list[i]) + ","
            min_line = min_line + "Min," + self.StrTransAndRot(self.min_trans_list[i], self.min_rot_list[i]) + ","
            max_line = max_line + "Max," + self.StrTransAndRot(self.max_trans_list[i], self.max_rot_list[i]) + ","

        # prepare header line
        frame_id_line =  "Base Topic," + str(self.subscribed_topic) + ", ,"
        header_line = ""
        for target in self.target_frame_id_list:
            frame_id_line = frame_id_line + str(target) + ", , , , , , , ,"
            header_line = header_line + ","
            for header_ in csv_header:
                header_line = header_line + str(header_) + ","
            header_line = header_line + ","

        # concatinate each line
        result_line = frame_id_line + "\n" + success_rate_line + "\n" + blank_line + header_line + "\n" \
                      + average_line + "\n" + std_dev_line + "\n" + min_line + "\n" + max_line + "\n" + "\n" \
                      + "frame, time, ," + header_line + "\n"

        # write
        with open(self.output_csv, "r+") as f:
            f.write(result_line)

    def SaveScatterPlot(self, x, y, title="scatter", label=None, x_label=None, y_label=None):
        for i in range(len(y)):
            plt.scatter(x, y[i], label=label[i])
        plt.grid(True)
        plt.legend()
        if not title == "scatter":
            plt.title(title)
        if not x_label ==None:
            plt.xlabel(x_label)
        if not y_label ==None:
            plt.ylabel(y_label)

        plt.savefig(self.output_dir + title + ".png")
        plt.cla()

        if self.save_individual_result:
            for i in range(len(y)):
                plt.scatter(x, y[i], label=label[i])
                plt.grid(True)
                plt.legend()
                if not title == "scatter":
                    plt.title(title)
                if not x_label ==None:
                    plt.xlabel(x_label)
                if not y_label ==None:
                    plt.ylabel(y_label)

                individual_output_dir = self.output_dir + str(label[i]).replace("/", "-") + "/"
                if not os.path.exists(individual_output_dir):
                    os.makedirs(individual_output_dir)
                plt.savefig(individual_output_dir + title + ".png")
                plt.cla()

    def SaveBarGraphs(self, y, error=None,
                      title="bar", label=None, x_label=None, y_label=None):
        x = np.arange(len(y))
        plt.bar(x, y, tick_label=label, yerr=error)
        plt.grid(True)
        plt.legend()
        if not title == "bar":
            plt.title(title)
        if not x_label ==None:
            plt.xlabel(x_label)
        if not y_label ==None:
            plt.ylabel(y_label)

        plt.savefig(self.output_dir + "average_" + title + ".png")
        plt.cla()

    def SaveGraphs(self):
        x_label = "Time [s]"
        trans_labels = ["x", "y", "z"]
        unit = "m"
        for i in range(len(trans_labels)):
            y_label = trans_labels[i] + " [" + unit + "]"
            y = (self.trans_arrays[:, :, i]).transpose()
            self.SaveScatterPlot(self.time_list, y, title=trans_labels[i],
                                 label=self.target_frame_id_list, x_label=x_label, y_label=y_label)
            self.SaveBarGraphs(np.asarray(self.ave_trans_list).transpose()[i],
                               error=np.asarray(self.std_trans_list).transpose()[i], title=trans_labels[i],
                               label=self.target_frame_id_list, x_label=x_label, y_label=y_label)

        rot_labels = ["roll", "pitch", "yaw"]
        unit = "deg"
        for i in range(len(rot_labels)):
            y_label = rot_labels[i] + " [" + unit + "]"
            y = (self.rot_arrays[:, :, i]).transpose()
            self.SaveScatterPlot(self.time_list, y, title=rot_labels[i],
                                 label=self.target_frame_id_list, x_label=x_label, y_label=y_label)
            self.SaveBarGraphs(np.asarray(self.ave_rot_list).transpose()[i],
                               error=np.asarray(self.std_rot_list).transpose()[i], title=rot_labels[i],
                               label=self.target_frame_id_list, x_label=x_label, y_label=y_label)

    def Finish(self, received_frame, frame):
        print ("\nSaving results to "+ self.output_csv + "...")
        if self.write_summary:
            self.WriteFinalResults()
            self.SaveGraphs()
        print ("Done.")
        sys.exit()


if __name__ == '__main__':
    if len(sys.argv[1:]) < 1:
        description = ""
    else:
        description = sys.argv[1]
    node = TfRecorder(output_path, subscribed_topic, description)

    rospy.init_node('tf_recorder')
    listener = tf.TransformListener()

    rospy.Subscriber(subscribed_topic, Image, node.MsgCallback, listener)

    signal(SIGINT, node.Finish)
    rospy.spin()


